import React, { useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthNavigator from "./AuthNavigator";
import HomeNavigator from "./HomeNavigator";
import { UserContext } from "../contexts/User";

const AppNavigator = () => {
  const { user } = useContext(UserContext);
  console.log("===================================");
  console.log(user);
  return (
    <NavigationContainer>
      {user.uid ? <HomeNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

export default AppNavigator;

// App.js
// => AppNavigator.js
//   => Auth.js (Stack)
//    => Screen....
