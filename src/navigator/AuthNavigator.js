import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Signin from "../screens/Signin";
import Signup from "../screens/Signup";
import Profile from "../screens/Profile";

const Stack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Signin" component={Signin} />
      <Stack.Screen name="Signup" component={Signup} />
      {/* <Stack.Screen name="Profile" component={Profile} /> */}
    </Stack.Navigator>
  );
};

export default AuthNavigator;
