import React from "react";
import styled from "styled-components/native";

const View = styled.TouchableOpacity`
  width: 100px;
  height: 40px;
  background-color: black;
  justify-content: center;
  margin: 10px;
`;

const Title = styled.Text`
  color: white;
  text-align: center;
`;

// 원래 버튼
// title -> 버튼 안에 있는 문자열
// onPress -> 눌렀을 때... 함수
const Button = ({ title, onPress }) => {
  return (
    <View onPress={onPress}>
      <Title>{title}</Title>
    </View>
  );
};

export default Button;
