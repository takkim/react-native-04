import styled from "styled-components/native";

export default styled.TextInput`
  width: 80%;
  height: 50px;
  border: 1px solid darkgray;
  margin: 10px;
  padding: 0 10px;
`;
