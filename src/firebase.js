import firebase from "firebase/app";
import "firebase/auth";
import firebaseConfig from "../firebaseConfig";

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();

export const signin = async ({ email, password }) => {
  console.log(email, password);
  const { user } = await firebase
    .auth()
    .signInWithEmailAndPassword(email, password);
  return user;
};

export const signup = async ({ email, password }) => {
  const { user } = await firebase
    .auth()
    .createUserWithEmailAndPassword(email, password);
  return user;
};

export const getCurrentUser = firebase.auth().currentUser;

export const signout = async () => {
  return await firebase.auth().signOut();
};
