import React, { createContext } from "react";
import { useState } from "react/cjs/react.development";

const UserContext = createContext({
  user: { uid: null },
  setUser: () => {},
});

const UserProvider = ({ children }) => {
  const [user, setUserInfo] = useState({});
  const setUser = ({ uid }) => {
    console.log("!!!!!!!!!!!!!!!!!");
    console.log(uid);
    setUserInfo({ uid });
  };
  const value = { user, setUser };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};

export { UserContext, UserProvider };
