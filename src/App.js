import React from "react";
import { UserProvider } from "./contexts/User";
import AppNavigator from "./navigator/AppNavigator";

export default function App() {
  return (
    <UserProvider>
      <AppNavigator />
    </UserProvider>
  );
}
