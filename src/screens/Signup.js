import React, { useContext, useRef, useState } from "react";
import Container from "../components/Container";
import Header from "../components/Header";
import Input from "../components/Input";
import Button from "../components/Button";
import { signup } from "../firebase";
import { UserContext } from "../contexts/User";

const Signup = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const refPassword = useRef(null);
  const { setUser } = useContext(UserContext);
  return (
    <Container>
      <Header>회원가입</Header>
      <Input
        placeholder="Email..."
        returnKeyType="next"
        autoCapitalize="none"
        value={email}
        onChangeText={setEmail}
        onSubmitEditing={() => {
          refPassword.current.focus();
        }}
      />
      <Input
        ref={refPassword}
        placeholder="Password..."
        secureTextEntry={true}
        value={password}
        onChangeText={setPassword}
      />
      <Button
        title="회원가입"
        onPress={() => {
          signup({ email, password })
            .then((user) => {
              setUser(user);
            })
            .catch((error) => {
              alert(error);
            });
        }}
      />
    </Container>
  );
};

export default Signup;
