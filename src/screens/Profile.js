import React, { useContext } from "react";
import Button from "../components/Button";
import Container from "../components/Container";
import Header from "../components/Header";
import { UserContext } from "../contexts/User";
import { getCurrentUser, signout } from "../firebase";

const Profile = () => {
  const { setUser } = useContext(UserContext);
  return (
    <Container>
      <Header>{getCurrentUser.email}</Header>
      <Button
        title="로그아웃"
        onPress={async () => {
          signout().then(() => {
            console.log("로그아웃!!");
            setUser({});
          });
        }}
      />
    </Container>
  );
};

export default Profile;
