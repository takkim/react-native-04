import React, { useContext, useRef, useState } from "react";
import Container from "../components/Container";
import Header from "../components/Header";
import Input from "../components/Input";
import Button from "../components/Button";
import { signin } from "../firebase";
import { UserContext } from "../contexts/User";
const Signin = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const refPassword = useRef(null);
  const { setUser } = useContext(UserContext);

  return (
    <Container>
      <Header>로그인</Header>
      <Input
        placeholder="Email..."
        returnKeyType="next"
        autoCapitalize="none"
        value={email}
        onChangeText={setEmail}
        onSubmitEditing={() => {
          refPassword.current.focus();
        }}
      />
      <Input
        ref={refPassword}
        placeholder="Password..."
        secureTextEntry={true}
        value={password}
        onChangeText={setPassword}
      />
      <Button
        title="로그인!"
        onPress={async () => {
          signin({ email, password })
            .then((user) => {
              console.log(user.uid, "signin");
              setUser(user);
              // navigation.navigate("Profile", user);
            })
            .catch((error) => {
              alert(error);
            });
        }}
      />
      <Button
        title="회원가입"
        onPress={() => {
          navigation.navigate("Signup");
        }}
      />
    </Container>
  );
};

export default Signin;
